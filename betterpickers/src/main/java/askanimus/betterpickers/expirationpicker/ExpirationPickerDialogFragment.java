package askanimus.betterpickers.expirationpicker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import askanimus.betterpickers.OnDialogDismissListener;
import askanimus.betterpickers.R;

import java.util.Vector;

/**
 * Dialog to set alarm time.
 */
public class ExpirationPickerDialogFragment extends DialogFragment {

    private static final String REFERENCE_KEY = "ExpirationPickerDialogFragment_ReferenceKey";
    private static final String THEME_RES_ID_KEY = "ExpirationPickerDialogFragment_ThemeResIdKey";
    private static final String MONTH_KEY = "ExpirationPickerDialogFragment_MonthKey";
    private static final String YEAR_KEY = "ExpirationPickerDialogFragment_YearKey";
    private static final String MINIMUM_YEAR_KEY = "ExpirationPickerDialogFragment_MinimumYearKey";
    private static final String MAXIMUM_YEAR_KEY = "ExpirationPickerDialogFragment_MaximumYearKey";
    private static final String MINIMUM_MONTH_KEY = "ExpirationPickerDialogFragment_MinimumMonthKey";
    private static final String MAXIMUM_MONTH_KEY = "ExpirationPickerDialogFragment_MaximumMonthKey";

    private ExpirationPicker mPicker;

    //private int mMonthOfYear = -1;
    //private int mYear = 0;
    private int mMinimumYear = 0;
    private int mMaximumYear = 0;
    private int mMinimumMonth = 0;
    private int mMaximumMonth = 0;

    private int mReference = -1;
    private int mTheme = -1;
    private int mDialogBackgroundResId;
    private Vector<ExpirationPickerDialogHandler> mExpirationPickerDialogHandlers = new Vector<ExpirationPickerDialogHandler>();
    private ColorStateList mTextColor;
    private OnDialogDismissListener mDismissCallback;

    /**
     * Create an instance of the Picker (used internally)
     *
     * @param reference    an (optional) user-defined reference, helpful when tracking multiple Pickers
     * @param themeResId   the style resource ID for theming
     * @param minimumYear  the year to set as the minimum
     * @param maximumYear  the year to set as the maximum
     * @param minimumMonth the month to set as the minimum
     * @param maximumMonth the month to set as the maximum
     * @return a Picker!
     */
    public static ExpirationPickerDialogFragment newInstance(
            int reference, int themeResId, Integer minimumYear, Integer maximumYear,
            Integer minimumMonth, Integer maximumMonth
            ) {
        final ExpirationPickerDialogFragment frag = new ExpirationPickerDialogFragment();
        Bundle args = new Bundle();
        args.putInt(REFERENCE_KEY, reference);
        args.putInt(THEME_RES_ID_KEY, themeResId);
        if (minimumYear != null) {
            args.putInt(MINIMUM_YEAR_KEY, minimumYear);
        }
        if (maximumYear != null) {
            args.putInt(MAXIMUM_YEAR_KEY, maximumYear);
        }
        if (minimumMonth != null) {
            args.putInt(MINIMUM_MONTH_KEY, minimumMonth);
        }
        if (maximumMonth != null) {
            args.putInt(MAXIMUM_MONTH_KEY, maximumMonth);
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(REFERENCE_KEY)) {
                mReference = args.getInt(REFERENCE_KEY);
            }
            if (args.containsKey(THEME_RES_ID_KEY)) {
                mTheme = args.getInt(THEME_RES_ID_KEY);
            }
            if (args.containsKey(MINIMUM_YEAR_KEY)) {
                mMinimumYear = args.getInt(MINIMUM_YEAR_KEY);
            }

            if (args.containsKey(MAXIMUM_YEAR_KEY)) {
                mMaximumYear = args.getInt(MAXIMUM_YEAR_KEY);
            }

            if (args.containsKey(MINIMUM_MONTH_KEY)) {
                mMinimumMonth = args.getInt(MINIMUM_MONTH_KEY);
            }

            if (args.containsKey(MAXIMUM_MONTH_KEY)) {
                mMaximumMonth = args.getInt(MAXIMUM_MONTH_KEY);
            }
        }

        setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        // Init defaults
        mTextColor = AppCompatResources.getColorStateList(requireContext(), R.color.dialog_text_color_holo_dark);
        mDialogBackgroundResId = R.drawable.dialog_full_holo_dark;

        if (mTheme != -1) {
            TypedArray a = requireActivity().getApplicationContext().obtainStyledAttributes(mTheme, R.styleable.BetterPickersDialogFragment);

            mTextColor = a.getColorStateList(R.styleable.BetterPickersDialogFragment_bpTextColor);
            mDialogBackgroundResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpDialogBackground, mDialogBackgroundResId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.expiration_picker_dialog, container, false);
        Button doneButton = (Button) view.findViewById(R.id.done_button);
        Button cancelButton = (Button) view.findViewById(R.id.cancel_button);

        cancelButton.setTextColor(mTextColor);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        doneButton.setTextColor(mTextColor);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (ExpirationPickerDialogHandler handler : mExpirationPickerDialogHandlers) {
                    handler.onDialogExpirationSet(mReference, mPicker.getYear(), mPicker.getMonthOfYear());
                }
                final Activity activity = getActivity();
                final Fragment fragment = getTargetFragment();
                if (activity instanceof ExpirationPickerDialogHandler) {
                    final ExpirationPickerDialogHandler act =
                            (ExpirationPickerDialogHandler) activity;
                    act.onDialogExpirationSet(mReference, mPicker.getYear(), mPicker.getMonthOfYear());
                } else if (fragment instanceof ExpirationPickerDialogHandler) {
                    final ExpirationPickerDialogHandler frag =
                            (ExpirationPickerDialogHandler) fragment;
                    frag.onDialogExpirationSet(mReference, mPicker.getYear(), mPicker.getMonthOfYear());
                }
                dismiss();
            }
        });

        mPicker = (ExpirationPicker) view.findViewById(R.id.expiration_picker);
        mPicker.setSetButton(doneButton);
        mPicker.setTheme(mTheme);

		if (mMinimumYear != 0) {
            mPicker.setMinYear(mMinimumYear);
        }

        if (mMaximumYear != 0){
            mPicker.setMaxYear(mMaximumYear);
        }

        if(mMinimumMonth != 0 && mMinimumYear != 0){
            mPicker.setMinDate(mMinimumYear, mMinimumMonth);
        }

        if(mMaximumMonth != 0 && mMaximumYear != 0){
            mPicker.setMaxDate(mMaximumYear, mMaximumMonth);
        }

        getDialog().getWindow().setBackgroundDrawableResource(mDialogBackgroundResId);
        return view;
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialoginterface) {
        super.onDismiss(dialoginterface);
        if (mDismissCallback != null) {
            mDismissCallback.onDialogDismiss(dialoginterface);
        }
    }

    public void setOnDismissListener(OnDialogDismissListener ondialogdismisslistener) {
        mDismissCallback = ondialogdismisslistener;
    }

    /**
     * This interface allows objects to register for the Picker's set action.
     */
    public interface ExpirationPickerDialogHandler {

        void onDialogExpirationSet(int reference, int year, int monthOfYear);
    }

    /**
     * Attach a Vector of handlers to be notified in addition to the Fragment's Activity and target Fragment.
     *
     * @param handlers a Vector of handlers
     */
    public void setExpirationPickerDialogHandlers(Vector<ExpirationPickerDialogHandler> handlers) {
        mExpirationPickerDialogHandlers = handlers;
    }
}