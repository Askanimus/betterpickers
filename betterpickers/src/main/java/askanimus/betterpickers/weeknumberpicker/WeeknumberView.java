package askanimus.betterpickers.weeknumberpicker;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.content.res.AppCompatResources;

import askanimus.betterpickers.R;
import askanimus.betterpickers.widget.PickerLinearLayout;
import askanimus.betterpickers.widget.UnderlinePageIndicatorPicker;
import askanimus.betterpickers.widget.ZeroTopPaddingTextView;

public class WeeknumberView extends PickerLinearLayout {

    private ZeroTopPaddingTextView mWeekLabel;
    private ZeroTopPaddingTextView mYearLabel;
    private final Typeface mAndroidClockMonoThin;
    private Typeface mOriginalNumberTypeface;
    private UnderlinePageIndicatorPicker mUnderlinePageIndicatorPicker;

    private ZeroTopPaddingTextView mSeperator;
    private ColorStateList mTitleColor;

    /**
     * Instantiate an weeknumberView
     *
     * @param context the Context in which to inflate the View
     */
    public WeeknumberView(Context context) {
        this(context, null);
    }

    /**
     * Instantiate an weeknumberView
     *
     * @param context the Context in which to inflate the View
     * @param attrs attributes that define the title color
     */
    public WeeknumberView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mAndroidClockMonoThin =
                Typeface.createFromAsset(context.getAssets(), "fonts/AndroidClockMono-Thin.ttf");
        mOriginalNumberTypeface =
                Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");

        // Init defaults
        mTitleColor = AppCompatResources.getColorStateList(context, R.color.dialog_text_color_holo_dark);

        setWillNotDraw(false);
    }

    /**
     * Set a theme and restyle the views. This View will change its title color.
     *
     * @param themeResId the resource ID for theming
     */
    public void setTheme(int themeResId) {
        if (themeResId != -1) {
            TypedArray a = getContext().obtainStyledAttributes(themeResId, R.styleable.BetterPickersDialogFragment);

            mTitleColor = a.getColorStateList(R.styleable.BetterPickersDialogFragment_bpTitleColor);
        }

        restyleViews();
    }

    private void restyleViews() {
        if (mWeekLabel != null) {
            mWeekLabel.setTextColor(mTitleColor);
        }
        if (mYearLabel != null) {
            mYearLabel.setTextColor(mTitleColor);
        }
        if (mSeperator != null) {
            mSeperator.setTextColor(mTitleColor);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mWeekLabel = (ZeroTopPaddingTextView) findViewById(R.id.week);
        mYearLabel = (ZeroTopPaddingTextView) findViewById(R.id.year_label);
        mSeperator = (ZeroTopPaddingTextView) findViewById(R.id.weeknumber_seperator);

        // Set both TextViews with thin font (for hyphen)
        if (mWeekLabel != null) {
            mWeekLabel.setTypeface(mAndroidClockMonoThin);
            mWeekLabel.updatePadding();
        }
        if (mYearLabel != null) {
            mYearLabel.setTypeface(mAndroidClockMonoThin);
        }
        if (mSeperator != null) {
            mSeperator.setTypeface(mAndroidClockMonoThin);
        }

        restyleViews();
    }

    /**
     * Set the date shown
     *
     * @param week a string representing the month of year
     * @param year an string representing the year
     */
    public void setWeeknumber(String week, String year) {
        if (mWeekLabel != null) {
            if (week.equals("")) {
                mWeekLabel.setText("--");
                mWeekLabel.setEnabled(false);
                mWeekLabel.updatePadding();
            } else {
                mWeekLabel.setText(week);
                mWeekLabel.setEnabled(true);
                mWeekLabel.updatePadding();
            }
        }
        if (mYearLabel != null) {
            if (year.equals("")) {
                mYearLabel.setText("----");
                mYearLabel.setEnabled(false);
                mYearLabel.updatePadding();
            } else {
                mYearLabel.setText(year);
                mYearLabel.setEnabled(true);
                mYearLabel.updatePadding();
            }
        }
    }

    /**
     * Allow attachment of the UnderlinePageIndicator
     *
     * @param indicator the indicator to attach
     */
    public void setUnderlinePage(UnderlinePageIndicatorPicker indicator) {
        mUnderlinePageIndicatorPicker = indicator;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mUnderlinePageIndicatorPicker.setTitleView(this);
    }

    /**
     * Set an onClickListener for notification
     *
     * @param mOnClickListener an OnClickListener from the parent
     */
    public void setOnClick(OnClickListener mOnClickListener) {
        mWeekLabel.setOnClickListener(mOnClickListener);
        mYearLabel.setOnClickListener(mOnClickListener);
    }

    /**
     * Get the week TextView
     *
     * @return the week TextView
     */
    public ZeroTopPaddingTextView getWeek() {
        return mWeekLabel;
    }

    /**
     * Get the year TextView
     *
     * @return the year TextView
     */
    public ZeroTopPaddingTextView getYear() {
        return mYearLabel;
    }

    @Override
    public View getViewAt(int index) {
        int actualIndex[] = {0, 2};

        if (index > actualIndex.length) {
            return null;
        } else {
            return getChildAt(actualIndex[index]);
        }
    }
}