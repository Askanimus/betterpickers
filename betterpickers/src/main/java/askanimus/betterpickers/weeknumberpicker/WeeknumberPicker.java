package askanimus.betterpickers.weeknumberpicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import askanimus.betterpickers.R;
import askanimus.betterpickers.Utils;
import askanimus.betterpickers.widget.UnderlinePageIndicatorPicker;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;


public class WeeknumberPicker extends LinearLayout implements Button.OnClickListener,
        Button.OnLongClickListener {

    private final static int WEEKNUMBER_WEEK_POSITION = 1;
    private final static int WEEKNUMBER_YEAR_POSITION = 0;

    private int mMaximumWeekOfYaer = 53;

    protected int mYearInputSize = 4;
    protected int mWeekInputSize = 2;
    protected int mWeekInput[] = new int[mWeekInputSize];
    protected int mYearInput[] = new int[mYearInputSize];
    protected int mWeekInputPointer = -1;
    protected int mYearInputPointer = -1;
    protected int mMinimumYear;
    protected int mMaximumYear;
    protected int mMinimumWeek;
    protected int mMaximumWeek;
    protected final Button mWeekNumbers[] = new Button[10];
    protected final Button mYearNumbers[] = new Button[10];
    protected Button mWeekLeft, mWeekRight;
    protected Button mYearLeft, mYearRight;
    protected UnderlinePageIndicatorPicker mKeyboardIndicator;
    protected ViewPager mKeyboardPager;
    protected KeyboardPagerAdapter mKeyboardPagerAdapter;
    protected ImageButton mDelete;
    protected askanimus.betterpickers.weeknumberpicker.WeeknumberView mEnteredWeeknumber;
    protected final Context mContext;
    //private char[] mDateFormatOrder;

    private static final String KEYBOARD_WEEK = "week";
    private static final String KEYBOARD_YEAR = "year";

    private static int sWeekKeyboardPosition = -1;
    private static int sYearKeyboardPosition = -1;

    private Button mSetButton;

    protected View mDivider;
    private ColorStateList mTextColor;
    private int mKeyBackgroundResId;
    private int mButtonBackgroundResId;
    private int mTitleDividerColor;
    private int mKeyboardIndicatorColor;
    private int mCheckDrawableSrcResId;
    private int mDeleteDrawableSrcResId;
    private int mTheme = -1;

    /**
     * Instantiates an WeeknumberPicker object
     *
     * @param context the Context required for creation
     */
    public WeeknumberPicker(Context context) {
        this(context, null);
    }

    /**
     * Instantiates an WeeknumberPicker object
     *
     * @param context the Context required for creation
     * @param attrs   additional attributes that define custom colors, selectors, and backgrounds.
     */
    public WeeknumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        //mDateFormatOrder = DateFormat.getDateFormatOrder(mContext);

        LayoutInflater layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(getLayoutId(), this);

        // Init defaults
        mTextColor = AppCompatResources.getColorStateList(context, R.color.dialog_text_color_holo_dark);
        mKeyBackgroundResId = R.drawable.key_background_dark;
        mButtonBackgroundResId = R.drawable.button_background_dark;
        mTitleDividerColor = getResources().getColor(R.color.default_divider_color_dark);
        mKeyboardIndicatorColor = getResources().getColor(R.color.default_keyboard_indicator_color_dark);
        mDeleteDrawableSrcResId = R.drawable.ic_backspace_dark;
        mCheckDrawableSrcResId = R.drawable.ic_check_dark;

        mMinimumYear = 0;
        mMaximumYear = 9999;
        mMinimumWeek = 0;
        mMaximumWeek = mMaximumWeekOfYaer;
    }

    protected int getLayoutId() {
        return R.layout.weeknumber_picker_view;
    }

    /**
     * Change the theme of the Picker
     *
     * @param themeResId the resource ID of the new style
     */
    public void setTheme(int themeResId) {
        mTheme = themeResId;
        if (mTheme != -1) {
            TypedArray a = mContext.obtainStyledAttributes(themeResId, R.styleable.BetterPickersDialogFragment);

            mTextColor = a.getColorStateList(R.styleable.BetterPickersDialogFragment_bpTextColor);
            mKeyBackgroundResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpKeyBackground,
                    mKeyBackgroundResId);
            mButtonBackgroundResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpButtonBackground,
                    mButtonBackgroundResId);
            mCheckDrawableSrcResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpCheckIcon,
                    mCheckDrawableSrcResId);
            mTitleDividerColor = a
                    .getColor(R.styleable.BetterPickersDialogFragment_bpTitleDividerColor, mTitleDividerColor);
            mKeyboardIndicatorColor = a
                    .getColor(R.styleable.BetterPickersDialogFragment_bpKeyboardIndicatorColor,
                            mKeyboardIndicatorColor);
            mDeleteDrawableSrcResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpDeleteIcon,
                    mDeleteDrawableSrcResId);
        }

        restyleViews();
    }

    /**
     * Set the minimum allowed year
     *
     * @param year the minimum year to restrict the picker to
     */
    public void setMinYear(int year) {
        mMinimumYear = year;
    }

    /**
     * Set the maximum allowed year
     *
     * @param year the maximum year to restrict the picker to
     */
    public void setMaxYear(int year) {
        mMaximumYear = year;
    }

    /**
     * Set the minimum allowed date
     *
     * @param year the minimum year to restrict the picker
     * @param week the minimum week to restrict the picker
     */
    public void setMinDate(int year, int week) {
        mMinimumYear = year;
        mMinimumWeek = week;
    }

    /**
     * Set the maximum allowed date
     *
     * @param year the maximum year to restrict the picker
     * @param week the maximum week to restrict the picker
     */
    public void setMaxDate(int year, int week) {
        mMaximumYear = year;
        mMaximumWeek = week;
    }

    private void restyleViews() {
        for (Button week : mWeekNumbers) {
            if (week != null) {
                week.setTextColor(mTextColor);
                week.setBackgroundResource(mKeyBackgroundResId);
            }
        }
        for (Button yearNumber : mYearNumbers) {
            if (yearNumber != null) {
                yearNumber.setTextColor(mTextColor);
                yearNumber.setBackgroundResource(mKeyBackgroundResId);
            }
        }
        if (mKeyboardIndicator != null) {
            mKeyboardIndicator.setSelectedColor(mKeyboardIndicatorColor);
        }
        if (mDivider != null) {
            mDivider.setBackgroundColor(mTitleDividerColor);
        }
        if (mDelete != null) {
            mDelete.setBackgroundResource(mButtonBackgroundResId);
            mDelete.setImageDrawable(AppCompatResources.getDrawable(mContext, mDeleteDrawableSrcResId));
        }
        if (mYearLeft != null) {
            mYearLeft.setTextColor(mTextColor);
            mYearLeft.setBackgroundResource(mKeyBackgroundResId);
        }
        if (mYearRight != null) {
            mYearRight.setTextColor(mTextColor);
            mYearRight.setBackgroundResource(mKeyBackgroundResId);
        }
        if (mEnteredWeeknumber != null) {
            mEnteredWeeknumber.setTheme(mTheme);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mDivider = findViewById(R.id.divider);

        Arrays.fill(mYearInput, 0);

        mKeyboardIndicator = (UnderlinePageIndicatorPicker) findViewById(R.id.keyboard_indicator);
        mKeyboardPager = (ViewPager) findViewById(R.id.keyboard_pager);
        mKeyboardPager.setOffscreenPageLimit(2);
        mKeyboardPagerAdapter = new KeyboardPagerAdapter(
                (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        mKeyboardPager.setAdapter(mKeyboardPagerAdapter);
        mKeyboardIndicator.setViewPager(mKeyboardPager);
        mKeyboardPager.setCurrentItem(0);

        mEnteredWeeknumber = (askanimus.betterpickers.weeknumberpicker.WeeknumberView) findViewById(R.id.date_text);
        mEnteredWeeknumber.setTheme(mTheme);
        mEnteredWeeknumber.setUnderlinePage(mKeyboardIndicator);
        mEnteredWeeknumber.setOnClick(this);

        mDelete = (ImageButton) findViewById(R.id.delete);
        mDelete.setOnClickListener(this);
        mDelete.setOnLongClickListener(this);

        setLeftRightEnabled();
        updateKeypad();
    }

    private class KeyboardPagerAdapter extends PagerAdapter {

        private final LayoutInflater mInflater;

        public KeyboardPagerAdapter(LayoutInflater inflater) {
            super();
            mInflater = inflater;
        }

        /**
         * inflate the week, or year keyboard
         *
         * @param collection the ViewPager collection group
         * @param position   the position within the ViewPager
         * @return an inflated View representing the keyboard for this position
         */
        @NonNull
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            View view;
            if (position == WEEKNUMBER_WEEK_POSITION) {
                // Weeks
                sWeekKeyboardPosition = position;
                view = mInflater.inflate(R.layout.keyboard, collection, false);
                View v1 = view.findViewById(R.id.first);
                View v2 = view.findViewById(R.id.second);
                View v3 = view.findViewById(R.id.third);
                View v4 = view.findViewById(R.id.fourth);

                mWeekNumbers[1] = (Button) v1.findViewById(R.id.key_left);
                mWeekNumbers[2] = (Button) v1.findViewById(R.id.key_middle);
                mWeekNumbers[3] = (Button) v1.findViewById(R.id.key_right);

                mWeekNumbers[4] = (Button) v2.findViewById(R.id.key_left);
                mWeekNumbers[5] = (Button) v2.findViewById(R.id.key_middle);
                mWeekNumbers[6] = (Button) v2.findViewById(R.id.key_right);

                mWeekNumbers[7] = (Button) v3.findViewById(R.id.key_left);
                mWeekNumbers[8] = (Button) v3.findViewById(R.id.key_middle);
                mWeekNumbers[9] = (Button) v3.findViewById(R.id.key_right);

                mWeekLeft = (Button) v4.findViewById(R.id.key_left);
                mWeekLeft.setTextColor(mTextColor);
                mWeekLeft.setBackgroundResource(mKeyBackgroundResId);
                mWeekNumbers[0] = (Button) v4.findViewById(R.id.key_middle);
                mWeekRight = (Button) v4.findViewById(R.id.key_right);
                mWeekRight.setTextColor(mTextColor);
                mWeekRight.setBackgroundResource(mKeyBackgroundResId);


                for (int i = 0; i < 10; i++) {
                    mWeekNumbers[i].setOnClickListener(WeeknumberPicker.this);
                    mWeekNumbers[i].setText(String.format(Locale.getDefault(), "%d", i));
                    mWeekNumbers[i].setTextColor(mTextColor);
                    mWeekNumbers[i].setBackgroundResource(mKeyBackgroundResId);
                    mWeekNumbers[i].setTag(R.id.date_keyboard, KEYBOARD_WEEK);
                    mWeekNumbers[i].setTag(R.id.numbers_key, i);
                }
            } else if (position == WEEKNUMBER_YEAR_POSITION) {
                // Year
                sYearKeyboardPosition = position;
                view = mInflater.inflate(R.layout.keyboard, collection, false);
                View v1 = view.findViewById(R.id.first);
                View v2 = view.findViewById(R.id.second);
                View v3 = view.findViewById(R.id.third);
                View v4 = view.findViewById(R.id.fourth);

                mYearNumbers[1] = (Button) v1.findViewById(R.id.key_left);
                mYearNumbers[2] = (Button) v1.findViewById(R.id.key_middle);
                mYearNumbers[3] = (Button) v1.findViewById(R.id.key_right);

                mYearNumbers[4] = (Button) v2.findViewById(R.id.key_left);
                mYearNumbers[5] = (Button) v2.findViewById(R.id.key_middle);
                mYearNumbers[6] = (Button) v2.findViewById(R.id.key_right);

                mYearNumbers[7] = (Button) v3.findViewById(R.id.key_left);
                mYearNumbers[8] = (Button) v3.findViewById(R.id.key_middle);
                mYearNumbers[9] = (Button) v3.findViewById(R.id.key_right);

                mYearLeft = (Button) v4.findViewById(R.id.key_left);
                mYearLeft.setTextColor(mTextColor);
                mYearLeft.setBackgroundResource(mKeyBackgroundResId);
                mYearNumbers[0] = (Button) v4.findViewById(R.id.key_middle);
                mYearRight = (Button) v4.findViewById(R.id.key_right);
                mYearRight.setTextColor(mTextColor);
                mYearRight.setBackgroundResource(mKeyBackgroundResId);

                for (int i = 0; i < 10; i++) {
                    mYearNumbers[i].setOnClickListener(WeeknumberPicker.this);
                    mYearNumbers[i].setText(String.format(Locale.getDefault(), "%d", i));
                    mYearNumbers[i].setTextColor(mTextColor);
                    mYearNumbers[i].setBackgroundResource(mKeyBackgroundResId);
                    mYearNumbers[i].setTag(R.id.date_keyboard, KEYBOARD_YEAR);
                    mYearNumbers[i].setTag(R.id.numbers_key, i);
                }
            } else {
                view = new View(mContext);
            }
            collection.addView(view, 0);
            setLeftRightEnabled();
            if(collection.getChildCount() > 1) {
                updateKeypad();
            }

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }
    }

    /**
     * Update the delete button to determine whether it is able to be clicked.
     */
    public void updateDeleteButton() {
        boolean enabled = mWeekInputPointer != -1 || mYearInputPointer != -1;
        if (mDelete != null) {
            mDelete.setEnabled(enabled);
        }
    }

    @Override
    public void onClick(View v) {
        v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
        doOnClick(v);
        updateDeleteButton();
    }

    protected void doOnClick(View v) {
        if (v == mDelete) {
            // Delete is dependent on which keyboard
            switch (mKeyboardPager.getCurrentItem()) {
                case WEEKNUMBER_WEEK_POSITION:
                    if (mWeekInputPointer >= 0) {
                        for (int i = 0; i < mWeekInputPointer; i++) {
                            mWeekInput[i] = mWeekInput[i + 1];
                        }
                        mWeekInput[mWeekInputPointer] = 0;
                        mWeekInputPointer--;
                    } else if (mKeyboardPager.getCurrentItem() > 0) {
                        mKeyboardPager.setCurrentItem(mKeyboardPager.getCurrentItem() - 1, true);
                    }
                    break;
                case WEEKNUMBER_YEAR_POSITION:
                    if (mYearInputPointer >= 0) {
                        for (int i = 0; i < mYearInputPointer; i++) {
                            mYearInput[i] = mYearInput[i + 1];
                        }
                        mYearInput[mYearInputPointer] = 0;
                        mYearInputPointer--;
                    }
                    break;
            }
        } else if (v == mEnteredWeeknumber.getWeek()) {
            mKeyboardPager.setCurrentItem(sWeekKeyboardPosition);
        } else if (v == mEnteredWeeknumber.getYear()) {
            mKeyboardPager.setCurrentItem(sYearKeyboardPosition);
        } else if (v.getTag(R.id.date_keyboard).equals(KEYBOARD_WEEK)) {
            // A week number was pressed
            addClickedWeekNumber((Integer) v.getTag(R.id.numbers_key));
        } else if (v.getTag(R.id.date_keyboard).equals(KEYBOARD_YEAR)) {
            // A year number was pressed
            addClickedYearNumber((Integer) v.getTag(R.id.numbers_key));
        }
        updateKeypad();
    }

    @Override
    public boolean onLongClick(View v) {
        v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
        if (v == mDelete) {
            mDelete.setPressed(false);
            reset();
            setLeftRightEnabled();
            updateKeypad();
            return true;
        }
        return false;
    }

    private void updateKeypad() {
        // Update state of keypad
        // Update the number
        updateYear();
        updateWeekLabel();
        // enable/disable the "set" key
        enableSetButton();
        // Update the backspace button
        updateDeleteButton();
        // update the keys
        updateWeekKeys();
        updateYearKeys();
    }

    /**
     * Reset all inputs and dates, and scroll to the first shown keyboard.
     */
    public void reset() {
        for (int i = 0; i < mYearInputSize; i++) {
            mYearInput[i] = 0;
        }
        for (int i = 0; i < mWeekInputSize; i++) {
            mWeekInput[i] = 0;
        }
        mYearInputPointer = -1;
        mWeekInputPointer = -1;
        mKeyboardPager.setCurrentItem(0, true);
    }

    private void updateYear() {
        int x;
        if (mMaximumYear < 9999 && mMinimumYear > 0) {
            if (mYearInputPointer < 0) {
                x = mMinimumYear / 1000;
                if (x == (mMaximumYear / 1000)) {
                    addClickedYearNumber(x);
                    updateKeypad();
                }
            } else if (mYearInputPointer == 0) {
                x = (mMinimumYear % 1000) / 100;
                if (x == (mMaximumYear % 1000) / 100) {
                    addClickedYearNumber(x);
                    updateKeypad();
                }
            } else if (mYearInputPointer == 1) {
                x = (mMinimumYear % 100) / 10;
                if (x == (mMaximumYear % 100) / 10) {
                    addClickedYearNumber(x);
                    updateKeypad();
                }

            } else if (mYearInputPointer == 2) {
                x = mMinimumYear % 10;
                if (x == mMaximumYear % 10) {
                    addClickedYearNumber(x);
                    updateKeypad();
                }
            }
        }
    }

    @SuppressLint("DefaultLocale")
    private void updateWeekLabel() {
        String week;
        StringBuilder year;
        if (mWeekInputPointer < 0) {
            week = "";
        } else {
            week = String.format(
                    (mWeekInputPointer) == 1 ? "%02d" : "-%d",
                    getWeekOfYear());
        }
        if (mYearInputPointer < 0) {
            year = new StringBuilder("");
        } else {
            year = new StringBuilder(Integer.toString(getYear()));
            // Pad to 4 digits
            while (year.length() < 4) {
                if (mYearInputPointer >= year.length()) {
                    year.insert(0, "0");
                } else {
                    year.append("-");
                }
            }
        }
        mEnteredWeeknumber.setWeeknumber(week, year.toString());
    }

    private void setLeftRightEnabled() {
        if (mYearLeft != null) {
            mYearLeft.setEnabled(false);
        }
        if (mYearRight != null) {
            mYearRight.setEnabled(false);
        }
    }

    private void addClickedYearNumber(int val) {
        if (mYearInputPointer < mYearInputSize - 1) {
            for (int i = mYearInputPointer; i >= 0; i--) {
                mYearInput[i + 1] = mYearInput[i];
            }
            mYearInputPointer++;
            mYearInput[0] = val;

            if (mYearInputPointer == 3) {
                Calendar cal = Calendar.getInstance();
                cal.set(getYear(), 1, 1);
                mMaximumWeekOfYaer = cal.getActualMaximum(Calendar.WEEK_OF_YEAR);
                if (mMaximumWeek > mMaximumWeekOfYaer) {
                    mMaximumWeek = mMaximumWeekOfYaer;
                }
                mKeyboardPager.setCurrentItem(mKeyboardPager.getCurrentItem() + 1, true);
            }
        }
    }

    private void addClickedWeekNumber(int val) {
        if (mWeekInputPointer < mWeekInputSize - 1) {
            for (int i = mWeekInputPointer; i >= 0; i--) {
                mWeekInput[i + 1] = mWeekInput[i];
            }
            mWeekInputPointer++;
            mWeekInput[0] = val;
        }
    }

    /**
     * Enable/disable keys on the week key pad according to the data entered
     */
    private void updateWeekKeys() {
        int minKey = 0;
        int maxKey;
        if (mWeekInputPointer < 0) {
            maxKey = 5;
            if (getYear() == mMinimumYear && mMinimumWeek > 0) {
                minKey = mMinimumWeek / 10;
            }
            if (getYear() == mMaximumYear) {
                maxKey = mMaximumWeek / 10;
            }
            Utils.setKeyRange(mWeekNumbers, minKey, maxKey);
        } else if (mWeekInputPointer == 0) {
            maxKey = (mWeekInput[mWeekInputPointer] == 5)? mMaximumWeekOfYaer % 10 : 9;
            if (getYear() == mMinimumYear && mMinimumWeek / 10 == mWeekInput[mWeekInputPointer]) {
                minKey = Math.min(mMaximumWeekOfYaer, mMinimumWeek % 10);
            }
            if (getYear() == mMaximumYear && mMaximumWeek / 10 == mWeekInput[mWeekInputPointer]) {
                maxKey = Math.min(mMaximumWeekOfYaer, mMaximumWeek % 10);
            }
            Utils.setKeyRange(mWeekNumbers, minKey, maxKey);
        }
    }

    /**
     * Enable/disable keys on the year key pad according to the data entered
     */
    private void updateYearKeys() {
        int minKey = 0;
        int maxKey = 9;
        if(mYearInputPointer < 0){
            if (mMinimumYear == 0){
                Utils.setMaxKeyRange(mYearNumbers, mMaximumYear / 1000);
            } else if(mMaximumYear == 9999){
                Utils.setMinKeyRange(mYearNumbers, mMinimumYear / 1000);
            } else {
                Utils.setKeyRange(mYearNumbers, (mMinimumYear / 1000), (mMaximumYear / 1000));
            }
        } else if (mYearInputPointer == 0){
            if(mMinimumYear != 0 && mYearInput[0] == mMinimumYear / 1000){
                minKey = (mMinimumYear % 1000) / 100;
            }
            if (mMaximumYear != 0 && mYearInput[0] == mMaximumYear / 1000){
                maxKey = (mMaximumYear % 1000) / 100;
            }
            Utils.setKeyRange(mYearNumbers, minKey, maxKey);
        }else if (mYearInputPointer == 1) {
            if(mYearInput[1] * 10 + mYearInput[0] == mMinimumYear / 100){
                minKey = (mMinimumYear % 100) / 10;
            }
            if (mYearInput[1] * 10 + mYearInput[0] == mMaximumYear / 100){
                maxKey = (mMaximumYear % 100) / 10;
            }
            Utils.setKeyRange(mYearNumbers, minKey, maxKey);
        } else if (mYearInputPointer == 2) {
            if(mYearInput[2] * 100 + mYearInput[1] * 10 + mYearInput[0] == mMinimumYear / 10){
                minKey = mMinimumYear % 10;
            }
            if (mYearInput[2] * 100 + mYearInput[1] * 10 + mYearInput[0] == mMaximumYear / 10){
                maxKey = mMaximumYear % 10;
            }
            Utils.setKeyRange(mYearNumbers, minKey, maxKey);
        } else if (mYearInputPointer == 3) {
            Utils.setMaxKeyRange(mYearNumbers, -1);
        }
    }

    /**
     * Enables a range of numeric keys from zero to maxKey. The rest of the keys will be disabled
     *
     * @param maxKey the maximum key that can be pressed
     */
    private void setYearMaxKeyRange(int maxKey) {
        for (int i = 0; i < mYearNumbers.length; i++) {
            if (mYearNumbers[i] != null) {
                mYearNumbers[i].setEnabled(i <= maxKey);
            }
        }
    }

    /**
     * Enables a range of numeric keys from minKey to maxKey. The rest of the keys will be disabled
     *
     * @param minKey the minimum key that can be pressed
     * @param maxKey the maximum key that can be pressed
     */
    private void setYearKeyRange(int minKey, int maxKey) {
        for (int i = 0; i < mYearNumbers.length; i++) {
            if (mYearNumbers[i] != null) {
                mYearNumbers[i].setEnabled(i <= maxKey && i >= minKey );
            }
        }
    }

    /**
     * Enables a range of numeric keys from minKey up. The rest of the keys will be disabled
     *
     * @param minKey the minimum key that can be pressed
     */
    private void setYearMinKeyRange(int minKey) {
        for (int i = 0; i < mYearNumbers.length; i++) {
            if (mYearNumbers[i] != null) {
                mYearNumbers[i].setEnabled(i >= minKey);
            }
        }
    }

    /**
     * Enables a range of numeric keys from zero to maxKey. The rest of the keys will be disabled
     *
     * @param maxKey the maximum key that can be pressed
     */
    private void setWeekMaxKeyRange(int maxKey) {
        for (int i = 0; i < mWeekNumbers.length; i++) {
            if (mWeekNumbers[i] != null) {
                mWeekNumbers[i].setEnabled(i <= maxKey);
            }
        }
    }

    /**
     * Enables a range of numeric keys from minKey to maxKey. The rest of the keys will be disabled
     *
     * @param minKey the minimum key that can be pressed
     * @param maxKey the maximum key that can be pressed
     */
    private void setWeekKeyRange(int minKey, int maxKey) {
        for (int i = 0; i < mWeekNumbers.length; i++) {
            if (mWeekNumbers[i] != null) {
                mWeekNumbers[i].setEnabled(i <= maxKey && i >= minKey );
            }
        }
    }

    /**
     * Enables a range of numeric keys from minKey up. The rest of the keys will be disabled
     *
     * @param minKey the minimum key that can be pressed
     */
    private void setWeekMinKeyRange(int minKey) {
        for (int i = 0; i < mWeekNumbers.length; i++) {
            if (mWeekNumbers[i] != null) {
                mWeekNumbers[i].setEnabled(i >= minKey);
            }
        }
    }

    /**
     * Enable/disable the "Set" button
     */
    private void enableSetButton() {
        if (mSetButton == null) {
            return;
        }
        int weekOfYear = (getYear() * 100) + getWeekOfYear();
        mSetButton.setEnabled(
                getWeekOfYear() != 0
                        && weekOfYear <= (mMaximumYear * 100) + mMaximumWeek
                        && weekOfYear >= (mMinimumYear * 100) + mMinimumWeek
        );
    }

    /**
     * Expose the set button to allow communication with the parent Fragment.
     *
     * @param b the parent Fragment's "Set" button
     */
    public void setSetButton(Button b) {
        mSetButton = b;
        enableSetButton();
    }

    /**
     * Returns the year as currently inputted by the user.
     *
     * @return the inputted year
     */
    public int getYear() {
        return mYearInput[3] * 1000 + mYearInput[2] * 100 + mYearInput[1] * 10 + mYearInput[0];
    }

    /**
     * Returns the week of year as currently inputted by the user.
     *
     * @return the inputed month
     */
    public int getWeekOfYear() {
        return mWeekInput[1] * 10 + mWeekInput[0];
    }

    @Override
    public Parcelable onSaveInstanceState() {
        final Parcelable parcel = super.onSaveInstanceState();
        final SavedState state = new SavedState(parcel);
        state.mWeekInput = mWeekInput;
        state.mYearInput = mYearInput;
        state.mYearInputPointer = mYearInputPointer;
        return state;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        final SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());

        mYearInputPointer = savedState.mYearInputPointer;
        mYearInput = savedState.mYearInput;
        if (mYearInput == null) {
            mYearInput = new int[mYearInputSize];
            mYearInputPointer = -1;
        }
        mWeekInput = savedState.mWeekInput;
        if (mWeekInput == null) {
            mWeekInput = new int[mWeekInputSize];
            mWeekInputPointer = -1;
        }
        updateKeypad();
    }

    private static class SavedState extends BaseSavedState {

        int mYearInputPointer;
        int mWeekInputPointer;
        int[] mYearInput;
        int[] mWeekInput;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            mYearInputPointer = in.readInt();
            in.readIntArray(mYearInput);
            mWeekInputPointer = in.readInt();
            in.readIntArray(mWeekInput);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(mYearInputPointer);
            dest.writeIntArray(mYearInput);
            dest.writeInt(mWeekInputPointer);
            dest.writeIntArray(mWeekInput);
        }

        public static final Creator<SavedState> CREATOR
                = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
