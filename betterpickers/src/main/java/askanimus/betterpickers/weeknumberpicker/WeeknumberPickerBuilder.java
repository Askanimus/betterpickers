package askanimus.betterpickers.weeknumberpicker;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import askanimus.betterpickers.OnDialogDismissListener;
import askanimus.betterpickers.weeknumberpicker.WeeknumberPickerDialogFragment.WeeknumberPickerDialogHandler;

import java.util.Calendar;
import java.util.Vector;

/**
 * @author Yuki Nishijima
 */
public class WeeknumberPickerBuilder {

    private FragmentManager manager; // Required
    private Integer styleResId; // Required
    private Fragment targetFragment;
    private Integer minimumYear;
    private Integer maximumYear;
    private Integer minimumWeek;
    private Integer maximumWeek;
    private int mReference = -1;
    private Vector<WeeknumberPickerDialogHandler> mWeeknumberPickerDialogHandlers = new Vector<WeeknumberPickerDialogHandler>();
    private OnDialogDismissListener mOnDismissListener;

    /**
     * Attach a FragmentManager. This is required for creation of the Fragment.
     *
     * @param manager the FragmentManager that handles the transaction
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setFragmentManager(FragmentManager manager) {
        this.manager = manager;
        return this;
    }

    /**
     * Attach a style resource ID for theming. This is required for creation of the Fragment. Two stock styles are
     * provided using R.style.BetterPickersDialogFragment and R.style.BetterPickersDialogFragment.Light
     *
     * @param styleResId the style resource ID to use for theming
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setStyleResId(int styleResId) {
        this.styleResId = styleResId;
        return this;
    }

    /**
     * Attach a target Fragment. This is optional and useful if creating a Picker within a Fragment.
     *
     * @param targetFragment the Fragment to attach to
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setTargetFragment(Fragment targetFragment) {
        this.targetFragment = targetFragment;
        return this;
    }

    /**
     * Attach a reference to this Picker instance. This is used to track multiple pickers, if the user wishes.
     *
     * @param reference a user-defined int intended for Picker tracking
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setReference(int reference) {
        this.mReference = reference;
        return this;
    }

    /**
     * Set the minimum year that the user is allowed to pick. The default is the current year.
     *
     * @param year the year to set as the minimum
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setMinYear(int year) {
        this.minimumYear = year;
        return this;
    }

    /**
     * Set the maximum year that the user is allowed to pick. The default is the current year.
     *
     * @param year the year to set as the maximum
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setMaxYear(int year) {
        this.maximumYear = year;
        return this;
    }

    /**
     * Set the minimum date that the user is allowed to pick. The default is the current year.
     *
     * @param year the year to set as the minimum
     * @param week the week to set as the minimum
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setMinDate(int year, int week) {
        this.minimumYear = year;
        this.minimumWeek = week;
        return this;
    }

    /**
     * Set the maximum date that the user is allowed to pick. The default is the current year.
     *
     * @param year the year to set as the maximum
     * @param week the week to set as the maximum
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder setMaxDate(int year, int week) {
        this.maximumYear = year;
        this.maximumWeek = week;
        return this;
    }


    /*
     * Pre-set a zero-indexed month of year. This is highly frowned upon as it contributes to user confusion.  The
     * Pickers do a great job of making input quick and easy, and thus it is preferred to always start with a blank
     * slate.
     *
     * @param weekOfYear the zero-indexed month of year to pre-set
     * @return the current Builder object
     */
   /* public WeeknumberPickerBuilder setWeekOfYear(int weekOfYear) {
        this.weekOfYear = weekOfYear;
        return this;
    }*/

    /*
     * Pre-set a year. This is highly frowned upon as it contributes to user confusion.  The Pickers do a great job of
     * making input quick and easy, and thus it is preferred to always start with a blank slate.
     *
     * @param year the year to pre-set
     * @return the current Builder object
     */
    /*public WeeknumberPickerBuilder setYear(int year) {
        this.year = year;
        return this;
    }*/

    /**
     * Attach universal objects as additional handlers for notification when the Picker is set. For most use cases, this
     * method is not necessary as attachment to an Activity or Fragment is done automatically.  If, however, you would
     * like additional objects to subscribe to this Picker being set, attach Handlers here.
     *
     * @param handler an Object implementing the appropriate Picker Handler
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder addWeeknumberPickerDialogHandler(WeeknumberPickerDialogHandler handler) {
        this.mWeeknumberPickerDialogHandlers.add(handler);
        return this;
    }

    /**
     * Remove objects previously added as handlers.
     *
     * @param handler the Object to remove
     * @return the current Builder object
     */
    public WeeknumberPickerBuilder removeWeeknumberPickerDialogHandler(WeeknumberPickerDialogHandler handler) {
        this.mWeeknumberPickerDialogHandlers.remove(handler);
        return this;
    }

    /**
     * Instantiate and show the Picker
     */
    public void show() {
        if (manager == null || styleResId == null) {
            Log.e("WeeknumberPickerBuilder", "setFragmentManager() and setStyleResId() must be called.");
            return;
        }
        FragmentTransaction ft = manager.beginTransaction();
        final Fragment prev = manager.findFragmentByTag("weeknumber_dialog");
        if (prev != null) {
            ft.remove(prev).commit();
            ft = manager.beginTransaction();
        }
        ft.addToBackStack(null);

        final askanimus.betterpickers.weeknumberpicker.WeeknumberPickerDialogFragment fragment = askanimus.betterpickers.weeknumberpicker.WeeknumberPickerDialogFragment
                .newInstance(mReference, styleResId, minimumYear, maximumYear, minimumWeek, maximumWeek);
        if (targetFragment != null) {
            fragment.setTargetFragment(targetFragment, 0);
        }
        fragment.setWeeknumberPickerDialogHandlers(mWeeknumberPickerDialogHandlers);
        fragment.setOnDismissListener(mOnDismissListener);
        fragment.show(ft, "weeknumber_dialog");
    }

    public WeeknumberPickerBuilder setOnDismissListener(OnDialogDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
        return this;
    }
}
