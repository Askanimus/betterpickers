package askanimus.betterpickers.weeknumberpicker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import askanimus.betterpickers.OnDialogDismissListener;
import askanimus.betterpickers.R;

import java.util.Calendar;
import java.util.Vector;

/**
 * Dialog to set alarm time.
 */
public class WeeknumberPickerDialogFragment extends DialogFragment {

    private static final String REFERENCE_KEY = "WeeknumberPickerDialogFragment_ReferenceKey";
    private static final String THEME_RES_ID_KEY = "WeeknumberPickerDialogFragment_ThemeResIdKey";
    private static final String MINIMUM_YEAR_KEY = "WeeknumberPickerDialogFragment_MinimumYearKey";
    private static final String MAXIMUM_YEAR_KEY = "WeeknumberPickerDialogFragment_MaximumYearKey";
    private static final String MINIMUM_WEEK_KEY = "WeeknumberPickerDialogFragment_MinimumWeekKey";
    private static final String MAXIMUM_WEEK_KEY = "WeeknumberPickerDialogFragment_MaximumWeekKey";

    private WeeknumberPicker mPicker;

    private int mMinimumYear = 0;
    private int mMaximumYear = 0;
    private int mMinimumWeek = 0;
    private int mMaximumWeek = 0;

    private int mReference = -1;
    private int mTheme = -1;
    private int mDialogBackgroundResId;
    private Vector<WeeknumberPickerDialogHandler> mWeeknumberPickerDialogHandlers = new Vector<>();
    private ColorStateList mTextColor;
    private OnDialogDismissListener mDismissCallback;

    /**
     * Create an instance of the Picker (used internally)
     *
     * @param reference   an (optional) user-defined reference, helpful when tracking multiple Pickers
     * @param themeResId  the style resource ID for theming
     * @param maximumYear  the year to set as the maximum
     * @param minimumWeek the week to set as the minimum
     * @param maximumWeek the week to set as the maximum
     * @return a Picker!
     */
    public static WeeknumberPickerDialogFragment newInstance(
            int reference, int themeResId, Integer minimumYear, Integer maximumYear,
            Integer minimumWeek, Integer maximumWeek
    ) {
        final WeeknumberPickerDialogFragment frag = new WeeknumberPickerDialogFragment();
        Bundle args = new Bundle();
        args.putInt(REFERENCE_KEY, reference);
        args.putInt(THEME_RES_ID_KEY, themeResId);
        if (minimumYear != null) {
            args.putInt(MINIMUM_YEAR_KEY, minimumYear);
        }
        if (maximumYear != null) {
            args.putInt(MAXIMUM_YEAR_KEY, maximumYear);
        }
        if (minimumWeek != null) {
            args.putInt(MINIMUM_WEEK_KEY, minimumWeek);
        }
        if (maximumWeek != null) {
            args.putInt(MAXIMUM_WEEK_KEY, maximumWeek);
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(REFERENCE_KEY)) {
                mReference = args.getInt(REFERENCE_KEY);
            }
            if (args.containsKey(THEME_RES_ID_KEY)) {
                mTheme = args.getInt(THEME_RES_ID_KEY);
            }
            if (args.containsKey(MINIMUM_YEAR_KEY)) {
                mMinimumYear = args.getInt(MINIMUM_YEAR_KEY);
            }

            if (args.containsKey(MAXIMUM_YEAR_KEY)) {
                mMaximumYear = args.getInt(MAXIMUM_YEAR_KEY);
            }

            if (args.containsKey(MINIMUM_WEEK_KEY)) {
                mMinimumWeek = args.getInt(MINIMUM_WEEK_KEY);
            }

            if (args.containsKey(MAXIMUM_WEEK_KEY)) {
                mMaximumWeek = args.getInt(MAXIMUM_WEEK_KEY);
            }
        }

        setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        // Init defaults
        mTextColor = AppCompatResources.getColorStateList(requireContext(), R.color.dialog_text_color_holo_dark);
        mDialogBackgroundResId = R.drawable.dialog_full_holo_dark;

        if (mTheme != -1) {
            TypedArray a = requireActivity().getApplicationContext().obtainStyledAttributes(mTheme, R.styleable.BetterPickersDialogFragment);

            mTextColor = a.getColorStateList(R.styleable.BetterPickersDialogFragment_bpTextColor);
            mDialogBackgroundResId = a.getResourceId(R.styleable.BetterPickersDialogFragment_bpDialogBackground, mDialogBackgroundResId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.weeknumber_picker_dialog, container, false);
        Button doneButton = (Button) view.findViewById(R.id.done_button);
        Button cancelButton = (Button) view.findViewById(R.id.cancel_button);

        cancelButton.setTextColor(mTextColor);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        doneButton.setTextColor(mTextColor);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (WeeknumberPickerDialogHandler handler : mWeeknumberPickerDialogHandlers) {
                    handler.onDialogWeeknumberSet(mReference, mPicker.getYear(), mPicker.getWeekOfYear());
                }
                final Activity activity = getActivity();
                final Fragment fragment = getTargetFragment();
                if (activity instanceof WeeknumberPickerDialogHandler) {
                    final WeeknumberPickerDialogHandler act =
                            (WeeknumberPickerDialogHandler) activity;
                    act.onDialogWeeknumberSet(mReference, mPicker.getYear(), mPicker.getWeekOfYear());
                } else if (fragment instanceof WeeknumberPickerDialogHandler) {
                    final WeeknumberPickerDialogHandler frag =
                            (WeeknumberPickerDialogHandler) fragment;
                    frag.onDialogWeeknumberSet(mReference, mPicker.getYear(), mPicker.getWeekOfYear());
                }
                dismiss();
            }
        });

        mPicker = (WeeknumberPicker) view.findViewById(R.id.weeknumber_picker);
        mPicker.setSetButton(doneButton);
        mPicker.setTheme(mTheme);

		if (mMinimumYear != 0) {
            mPicker.setMinYear(mMinimumYear);
        }

        if (mMaximumYear != 0){
            mPicker.setMaxYear(mMaximumYear);
        }

        if(mMinimumWeek != 0 && mMinimumYear != 0){
            mPicker.setMinDate(mMinimumYear, mMinimumWeek);
        }

        if(mMaximumWeek != 0 && mMaximumYear != 0){
            mPicker.setMaxDate(mMaximumYear, mMaximumWeek);
        }

        getDialog().getWindow().setBackgroundDrawableResource(mDialogBackgroundResId);
        return view;
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialoginterface) {
        super.onDismiss(dialoginterface);
        if (mDismissCallback != null) {
            mDismissCallback.onDialogDismiss(dialoginterface);
        }
    }

    public void setOnDismissListener(OnDialogDismissListener ondialogdismisslistener) {
        mDismissCallback = ondialogdismisslistener;
    }

    /**
     * This interface allows objects to register for the Picker's set action.
     */
    public interface WeeknumberPickerDialogHandler {

        void onDialogWeeknumberSet(int reference, int year, int weekOfYear);
    }

    /**
     * Attach a Vector of handlers to be notified in addition to the Fragment's Activity and target Fragment.
     *
     * @param handlers a Vector of handlers
     */
    public void setWeeknumberPickerDialogHandlers(Vector<WeeknumberPickerDialogHandler> handlers) {
        mWeeknumberPickerDialogHandlers = handlers;
    }
}