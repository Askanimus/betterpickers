package askanimus.betterpickers;

import android.content.DialogInterface;

public interface OnDialogDismissListener {

    void onDialogDismiss(DialogInterface dialoginterface);
}
